const userService = require('../services/userService.js')
const authService = require('../services/authService.js');
const fs = require('fs');
// const path = require('path');
var multer = require('multer');

var upload = multer({ dest: 'usr/src/app/userPictures/' });

async function getAllUsers(req, res) {
    const users = await userService.findAll(req.db);

    users.forEach(function (v) { delete v.password }); // On n'enlève le mot de pass !

    const response = {
        'message': "List of users in Hospitech",
        'list': {
            'users': users,
        }
    }
    return res.status(200).send(response);
}

async function getProfile(req, res) {
    const authorization = req.headers.authorization;
    const user = await authService.authenticateToken(authorization);

    if (user === undefined) {
        return res.status(400).send({ message: 'Error user is not logged' })
    }
    let userObj = await userService.findByEmail(user.username, req.db);
    const response = {
        'message': "User profile successfully gotten",
        'content': {
            'user': userObj,
        }
    }

    return res.status(200).send(response);
}

async function editProfile(req, res) {
    
    if (req.body === undefined)
        return res.status(400).send({ message: 'No parameters founded.' })

    const result = await userService.updateUser(req.body.data, req.db);
    if (result === undefined)
        return res.status(400).send({ message: 'An error occur during user updating.' })
    else{
        const response = {
            'content': {
                'user': result
            },
            'message': 'User successfully updated.'
        }

        return res.status(200).send(response);
    }
}

async function register(req, res) {
    const { email, firstname, lastname, password, birthdate, num_ss, profilImg } = req.body;
    let role = 'ROLE_USER';

    if (email === undefined || password === undefined || firstname === undefined || lastname === undefined)
        return res.status(400).send({ message: 'Parameters missing.' })

    if (req.body.role && req.body.role === process.env.ADMIN_KEY) { // Il a la clée d'accès donc c'est un admin interne
        role = 'ROLE_SUPER_ADMIN';
    }

    const data = {
        'email': email,
        'password': password,
        'firstname': firstname,
        'lastname': lastname,
        'birthdate': birthdate,
        'num_ss': num_ss,
        'profilImg': profilImg,
        'role': role,
        'licence': "",
        'orders': []
    }
    const result = await userService.create(data, req.db);

    if (result === undefined)
        return res.status(400).send({ message: 'An error occur during user creation.' })

    const response = {
        'content': {
            'user': result
        },
        'message': 'User successfully authentificated.'
    }

    return res.status(201).send(response);
}

async function getUserLicence(req, res) {
    const authorization = req.headers.authorization;
    const user = await authService.authenticateToken(authorization);

    if (user === undefined) {
        return res.status(400).send({ message: 'Error user is not logged' })
    }
    let userObj = await userService.findByEmail(user.username, req.db);

    const response = {
        'message': "User profile successfully gotten",
        'content': {
            'licence': userObj.licence,
        }
    }
    return res.status(200).send(response);
}

async function updateUserLicence(req, res) {
    const authorization = req.headers.authorization;
    let user = await authService.authenticateToken(authorization);
    const newLicenceLvl = req.body.licence;
    const orders = req.body.orders;

    if (user === undefined) {
        return res.status(400).send({ message: 'Error user is not logged' })
    }
    const result = await userService.update(req.db, user, newLicenceLvl, orders);

    if (result === undefined)
        return res.status(400).send({ message: 'An error occur during user creation.' })

    const response = {
        'content': {
            'user': result
        },
        'message': 'User\'s Licence successfully Updated.'
    }
    return res.status(200).send(response);
}

async function getPaypalRedirect(req, res) {
    console.log("User data ==>", req.body);

    const response = {
        'message': "User Paypal Event successfully gotten",
        'content': {
            'paypal': "null",
        }
    }
    return res.status(200).send(response);
}

async function setPaypal(req, res) {
    let paypalUrl = process.env.PAYPAL_URL;
    // curl -v -X POST https://api-m.sandbox.paypal.com/v2/invoicing/generate-next-invoice-number \
    // -H "Content-Type: application/json" \
    // -H "Authorization: Bearer <Access-Token>"

    const response = {
        'message': "User Paypal Event successfully gotten",
        'content': {
            'paypal': "null",
        }
    }
    return res.status(200).send(response);
}

async function saveTmpPhoto(req, res, next) {
    if (req.file && req.file.destination && req.file.originalname) {
        if (fs.existsSync(req.file.destination + "/" + req.body.newname)) {
            res.status(200).send({
                message: 'Image Created.',
                imagename: req.body.newname
            });
            return;
        } else {
            res.status(400).send({ message: 'Almost An error occur during user creation, problem at parsing files' })
            return;
        }
    }
    else {
        res.status(400).send({ message: 'An error occur during user creation missing parameters.' })
    }
}

async function getUserPhoto(req, res) {
    const authorization = req.headers.authorization;
    const user = await authService.authenticateToken(authorization);

    if (user === undefined) {
        return res.status(400).send({ message: 'Error user is not logged' })
    }
    let userObj = await userService.findByEmail(user.username, req.db);
    let file = `${__dirname}/../usr/src/app/userPictures/${userObj.profilImg}`;
    return res.download(file);
}

module.exports = {
    getAllUsers,
    getProfile,
    editProfile,
    register,
    getUserLicence,
    updateUserLicence,
    getPaypalRedirect,
    saveTmpPhoto,
    getUserPhoto
}