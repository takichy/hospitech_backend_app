const express = require('express');
const router = express.Router();
// const dataFakerfile = require('../services/dataFaker')
// const dataFakerObj = new dataFakerfile.dataFaker();
const path = require('path');
var multer = require('multer');

const MIME_TYPES = {
  'image/jpg': 'jpg',
  'image/jpeg': 'jpg',
  'image/png': 'png'
};
// usr/src/app/userPictures
const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, './usr/src/app/userPictures');
  },
  filename: (req, file, callback) => {
    if (req.body.type_image && req.body.userMail) {
      const name = req.body.userMail + '_' + req.body.type_image + '_' + file.originalname.split(' ').join('_');
      const extension = MIME_TYPES[file.mimetype];
      req.body.newname = name;
      callback(null, name);
    }
  }
});


const isProd = false // à gérer avec une variable d'env pour savoir si on est en prod ou en dev

if (!isProd) {
  // dataFakerObj.Fill();
}

// db.test(DbLogged);

const authFunctions = require('./authRoutes.js');
const hospiFunctions = require('./hospiRoutes.js');
const userFunctions = require('./userRoutes.js');
const stockFunctions = require('./stockRoutes.js');
const docFunctions = require('./docRoutes.js');
const admissionFunctions = require ('./admissionRoutes.js')
// const settingFunctions = require('./settingsRoutes.js');

const none = () => {}

const setupRoutes = (routes) => {
  // Basic route format : { path: string, method: 'GET' || 'POST' || 'PUT' || 'DELETE', bind: function }
  routes.forEach(route => {
      if (route.method === 'GET')
        router.route(route.path).get(route.bind)
      if (route.method === 'POST')
        router.route(route.path).post(route.bind)
      if (route.method === 'PUT')
        router.route(route.path).put(route.bind)
      if (route.method === 'DELETE')
        router.route(route.path).delete(route.bind)
      if (route.method === 'POSTFILEUSERIMAGE') {
        router.post('/users/update_photo',  multer({storage: storage}).single('userImage'), userFunctions.saveTmpPhoto);
      }
        // router.route(route.path).post(route.bind)
  });
}

// User Routes
const userRoutes = new Array(
  { path: '/users', method: 'GET', bind: userFunctions.getAllUsers },
  { path: '/users/profile', method: 'GET', bind: userFunctions.getProfile },
  { path: '/users/profile', method: 'PUT', bind: userFunctions.editProfile },
  { path: '/users/register', method: 'POST', bind: userFunctions.register },
  { path: '/users/licence', method: 'GET', bind: userFunctions.getUserLicence },
  { path: '/users/photo', method: 'GET', bind: userFunctions.getUserPhoto },
  { path: '/users/update_licence', method: 'PUT', bind: userFunctions.updateUserLicence },
  { path: '/users/update_photo', method: 'POSTFILEUSERIMAGE', bind: userFunctions.saveTmpPhoto }
  )

// Doc Routes
const docRoutes = new Array(
  { path: '/docs/getdocs/:id_user', method: 'GET', bind: docFunctions.getDocsforUser },
  { path: '/docs/update_doc', method: 'POST', bind: docFunctions.insertDoc },
  { path: '/docs/:id', method: 'DELETE', bind: docFunctions.deleteOne }
)

// Doc Routes
const admissionRoutes = new Array(
  { path: '/admission', method: 'POST', bind: admissionFunctions.createAdmission},
)
  
// Auth Routes
const authRoutes = new Array(
  { path: '/users/login', method: 'POST', bind: authFunctions.login },
  { path: '/users/logout', method: 'POST', bind: authFunctions.logout },
  { path: '/users/auth/:provider', method: 'GET', bind: authFunctions.loginWithProvider },
  { path: '/users/auth/:provider/callback', method: 'GET', bind: none },
)

// Hospital Routes
const hospiRoutes = new Array(
  { path: '/hospital/create', method: 'POST', bind: hospiFunctions.create },
  { path: '/hospital', method: 'GET', bind: hospiFunctions.getAll },
  { path: '/hospital/:id', method: 'DELETE', bind: hospiFunctions.deleteOne }
);

// Settings
const settingsRoutes = new Array(
  // { path: '/settings', method: 'PUT', bind: settingFunctions.update },
  // { path: '/settings', method: 'GET', bind: settingFunctions.getSettings },
)

const paypalRoutes = new Array(
  { path: '/paypalRoutes', method: 'GET', bind: userFunctions.getPaypalRedirect }
)

const stockRoutes = new Array(
  { path: '/stock', method: 'GET', bind: stockFunctions.getAllStock },
  { path: '/stock/:id', method: 'GET', bind: stockFunctions.getMedication },
  { path: '/stock/create', method: 'POST', bind: stockFunctions.create },
  { path: '/stock/commandMedication', method: 'PUT', bind: stockFunctions.commandMedication },
  { path: '/stock/commandArrived', method: 'PUT', bind: stockFunctions.commandArrived },
  { path: '/stock/stockRemoval', method: 'PUT', bind: stockFunctions.stockRemoval },
)

setupRoutes(userRoutes)
setupRoutes(docRoutes)
setupRoutes(authRoutes)
setupRoutes(hospiRoutes)
setupRoutes(settingsRoutes)
setupRoutes(paypalRoutes)
setupRoutes(stockRoutes)

module.exports = router;
