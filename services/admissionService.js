// const Database = require('./database.js');
const user = require('../model/user').User;
var ObjectId = require('mongodb').ObjectId;

// let database = new Database();



// getOneSettingDocument('Users', '', UserId.uid);
async function findAll(db) {
  return await db.collection("admission").find({}).toArray();
}

async function create(data, db) {
  try {
    let result = await db.collection("admission").insertOne(data);
    if (result.insertedId)
        return result;
  }
  catch (e) {
    console.error(e);
  }
  return undefined;
}



module.exports = {
  create,
  findAll
};
