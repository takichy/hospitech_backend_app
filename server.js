const express = require('express')
const app = express()
const port = 8080

const dotenv = require('dotenv');
// get config vars
dotenv.config();
// access config var
process.env.TOKEN_SECRET;

const security = require('./config/security');

const router = require('./routes/router.js');

security.initializeCORS(app);

security.initializeCSRF(app, router)

const db = require('./services/Dbconnection');

const DbLogged = db.initialize().then(function (dataDbLogged) {
  dataDbLogged.collection('users').find().toArray(function (err, result) {
    if (err) throw err
      console.log(result)
  });

  // const bodyParser = express.json();

  var bodyParser = require('body-parser');
  // app.use((req, res, next) => {
  //   if(req.originalUrl == "/users/update_photo") {

  //     next();
  //   } 
  // });
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));
  app.use(function(req,res,next){
    req.db = dataDbLogged;
    next();
  });
  app.use(router);

  app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })
});
