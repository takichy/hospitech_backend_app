var ObjectId = require('mongodb').ObjectId;

async function createOne(data, db) {
    let result = await db.collection("hospitals").findOne({user : data.user, centername: data.centername});

    if (result) {
        return undefined;
    } 
    else {
        try {
            let result = await db.collection("hospitals").insertOne(data);
            if (result.insertedId)
                return result;
        }
        catch (e) {
            console.error(e);
        }
    }
    return undefined;    
}

async function getOne(data, db) {
    return undefined;    
}

async function getAll(data, db) {
    let user = data.user;
    return await db.collection("hospitals").find({user: user}).toArray();
}

async function deleteOne(data, db) {
    let user = data.user;
    var o_id = new ObjectId(data.centerid);
    let foundHospi = await db.collection("hospitals").find({user: user, _id: o_id}).toArray();
    if (!foundHospi)
        return undefined;
    try {
        db.collection("hospitals").deleteOne( { "_id" : ObjectId(data.centerid) } );
        return true;
    } catch (e) {
        print(e);
    }
    return undefined;
}

module.exports = {
    createOne,
    getOne,
    getAll,
    deleteOne
}