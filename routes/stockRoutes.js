const stockService = require('../services/stockService.js')
const authService = require('../services/authService.js');

async function getAllStock(req, res)
{
    const stock = await stockService.findAll(req.db);

    const response = {
        'message': "List of medication in stock", 
        'list': {
            'stock': stock,
        }
    }
    return res.status(200).send(response);
}

async function getMedication(req, res)
{
    const authorization = req.headers.authorization;
    const user = await authService.authenticateToken(authorization);

    if (user === undefined) {
        return res.status(400).send({message: 'Error user is not logged'})
    }
    let id = req.params.id;
    let medicationObj = await stockService.findById(id, req.db);
    console.log(medicationObj)
    const response = {
        'message': "Medication successfully find", 
        'content': {
            'medication': medicationObj,
        }
    }

    return res.status(200).send(response);
}

async function create(req, res)
{
    const authorization = req.headers.authorization;
    const user = await authService.authenticateToken(authorization);
    if (user === undefined) {
        return res.status(400).send({message: 'Error user is not logged'})
    }

    const {name, refNumber} = req.body;
    console.log(req.body)
    if (name === undefined || refNumber === undefined)
        return res.status(400).send({message: 'Parameters missing.'})

    if (req.body.role && req.body.role === process.env.ADMIN_KEY) { // Il a la clée d'accès donc c'est un admin interne
        role = 'ROLE_SUPER_ADMIN';
        const data = {
            'name': name,
            'refNumber': refNumber,
            'inStock': 0,
            'inCommand': 0,
        }
    
        const result = await stockService.create(data, req.db);
    
        if (result === undefined)
            return res.status(400).send({message: 'An error occur during user creation.'})
    
        const response = {
            'content': {
                'user': result
            },
            'message': 'medication successfully added.'
        }
        return res.status(201).send(response);
    } else {
        const response = {
            'content': {
                'medication': result
            },
            'message': 'Not allowed to add a new medication.'
        }
        return res.status(401).send(response);
    }
}

async function commandMedication(req, res) {

    const authorization = req.headers.authorization;
    const user = await authService.authenticateToken(authorization);

    if (user === undefined)
        return res.status(400).send({message: 'Error user is not logged'})
    if (req.body.role && req.body.role === process.env.ADMIN_KEY){

        if (req.body === undefined)
            return res.status(400).send({message: 'No parameters founded.'})

        const {id, command} = req.body;
        if (id === undefined || command === undefined)
            return res.status(400).send({message: 'Parameters missing.'})
            
        const data = {
            'id': req.body.id,
            'ref': req.body.ref,
            'command': req.body.command
        };

        stockService.command(data,req.db)

        const response = {
            'message': "Command successfully done."
        }

        return res.send(response);
    }
}

async function commandArrived(req, res) {

    const authorization = req.headers.authorization;
    const user = await authService.authenticateToken(authorization);

    if (user === undefined)
        return res.status(400).send({message: 'Error user is not logged'})
    if (req.body.role && req.body.role === process.env.ADMIN_KEY){

        if (req.body === undefined)
            return res.status(400).send({message: 'No parameters founded.'})

        const {id} = req.body;
        if (id === undefined)
            return res.status(400).send({message: 'Parameters missing.'})
            
        const data = {
            'id': req.body.id,
        };

        stockService.commandArrived(data,req.db)

        const response = {
            'message': "Command successfully arrived."
        }

        return res.send(response);
    }
}

async function stockRemoval(req, res) {

    const authorization = req.headers.authorization;
    const user = await authService.authenticateToken(authorization);

    if (user === undefined)
        return res.status(400).send({message: 'Error user is not logged'})
    if (req.body.role && req.body.role === process.env.ADMIN_KEY){

        if (req.body === undefined)
            return res.status(400).send({message: 'No parameters founded.'})

        const {id} = req.body;
        if (id === undefined)
            return res.status(400).send({message: 'Parameters missing.'})
            
        const data = {
            'id': req.body.id,
        };

        stockService.stockRemoval(data,req.db)

        const response = {
            'message': "Medication out of Stock."
        }

        return res.send(response);
    }
}

module.exports = {
    getAllStock,
    getMedication,
    commandMedication,
    commandArrived,
    stockRemoval,
    create,
}