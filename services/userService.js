// const Database = require('./database.js');
const user = require('../model/user').User;
var ObjectId = require('mongodb').ObjectId;

// let database = new Database();

async function findByEmail(email, database) {
  let result = await database.collection("users").findOne({ email: email });
  return result;
}

async function findById(id, database) {
  var o_id = new ObjectId(id);
  let result = await database.collection("users").findOne({ _id: o_id });
  return result;
}

// getOneSettingDocument('Users', '', UserId.uid);

async function findnoerror(userId) {
  // console.log(" really ? " + await database.getOneUserNoError('Users', userId));
  // return await database.getOneUserNoError('Users', userId);
}

async function findAll(db) {
  return await db.collection("users").find({}).toArray();
}

async function create(data, db) {
  // let User = new user(db);
  const bcrypt = require('bcryptjs');
  const saltRounds = 10;

  let password = data.password;
  data.password = bcrypt.hashSync(password, saltRounds);

  let result = await db.collection("users").findOne({ email: data.email });

  if (result) {
    return undefined;
  } else {
    try {
      let result = await db.collection("users").insertOne(data);
      if (result.insertedId)
        return result;
    }
    catch (e) {
      console.error(e);
    }
  }
  return undefined;
}

async function updateUser(data, db) {

  var o_id = new ObjectId(data.id);
  let result = await db.collection("users").findOne({ _id: o_id });

  if (!result) {
    return undefined;
  } else {
    try {
      let result = await db.collection("users").updateOne(
        { _id: o_id },
        {
          $set: {
            'email': data.email,
            'password': data.password,
            'firstname': data.firstname,
            'lastname': data.lastname,
            'birthdate': data.birthdate,
            'num_ss': data.num_ss,
            'profilImg': data.profilImg
          },
          $currentDate: { lastModified: true }
        });
      if (result.modifiedCount > 0)
        return result;
    }
    catch (e) {
      console.error(e);
    }
  }
  return undefined;


}

async function update(database, data, newLicence, orders) {
  try {
    const user = await findByEmail(data.username, database);
    let myorder = user.orders;
    myorder.push(orders);
    return await database.collection("users").updateOne(
      { _id: user._id },
      {
        $set: { "orders": myorder, "licence": newLicence },
        $currentDate: { lastModified: true }
      }
    )
  }
  catch (e) {
    console.error(e);
  }
  return;
}

function remove(userId) {
  // work in progress
  // return database.deleteDocument('Users', userId);
}

module.exports = {
  findByEmail,
  findById,
  create,
  updateUser,
  update,
  remove,
  findnoerror,
  findAll
};
