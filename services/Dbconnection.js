var MongoClient = require('mongodb').MongoClient

var userModel = require('../model/user');

exports.initialize = async () => {
  let db;
  try {
      // Build the connection String and create the database connection
      const Client = await MongoClient.connect(
          `mongodb://hospiroot:hospiroot@${process.env.INSTANCE_DB}:27017/?authSource=admin`,
          {
              useNewUrlParser: true,
              useUnifiedTopology: true
          }
      )
      // db global variable 
      db = Client.db("hospitech")
      db.createCollection("users", function(err, res) {
        if (err) {
          if (err.code == 48 && err.errmsg == "Collection already exists. NS: hospitech.users")
          console.error("Users Collection aldready exists, will not create it again");
        } else {
          console.log("Collection Users created!");
        }
        // db.close();
      }); 
      db.createCollection("documents", function(err, res) {
        if (err) {
          if (err.code == 48 && err.errmsg == "Collection already exists. NS: hospitech.documents")
          console.error("Documents Collection aldready exists, will not create it again");
        } else {
          console.log("Collection Documents created!");
        }
        // db.close();
      });
      db.createCollection("admission", function(err, res) {
        if (err) {
          if (err.code == 48 && err.errmsg == "Collection already exists. NS: hospitech.admission")
          console.error("Admission Collection aldready exists, will not create it again");
        } else {
          console.log("Collection Admission created!");
        }
        // db.close();
      });
      db.createCollection("hospitals", function(err, res) {
        if (err) {
          if (err.code == 48 && err.errmsg == "Collection already exists. NS: hospitech.hospitals")
          console.error("Hospitals Collection aldready exists, will not create it again");
        } else {
          console.log("Collection Hospitals created!");
        }
        // db.close();
      });
  }
  catch (err) {
      return console.log('Database Connection Error!', err.message)
  }
  return db;
}

exports.test = async (database) => {
  console.log("Test Data ==> ", await database);
};
