async function login(email, password, db) {
    
    let check;
    let user;

    try {
        const bcrypt = require('bcrypt');
        const saltRounds = 10;
        user = (await db.collection("users").findOne({email : email}));

        if (user.password)
            check = await bcrypt.compare(password, user.password)
    } catch (e) {
        return undefined;
    }

    console.log("no !!");
    if (check == false)
        return undefined;

    return await generateToken({username: user.email});
}

async function googleAuth (token) {
    return await database.signInWithGoogle(token);
}

async function generateToken(usermail) {
    const jwt = require('jsonwebtoken');
    let token = jwt.sign(usermail, process.env.TOKEN_SECRET, { expiresIn: '1800s' });

    return token;
}

async function authenticateToken(authHeaderParam) {
  const jwt = require('jsonwebtoken');
  const authorization = authHeaderParam
  const token = authorization && authorization.split(' ')[1]

  if (token == null) return;

    try {
        const decoded = jwt.verify(token, process.env.TOKEN_SECRET);
        return decoded;
    } catch(err) {
        console.error('err', err)
    }
    return;
}

module.exports = {
    login,
    // getUserFromAuthorization,
    authenticateToken,
    googleAuth
}