const user = require('../model/user').User;
const doc = require('../model/document').Document;
var ObjectId = require('mongodb').ObjectId;

async function createDoc(data, db) {

    try {
        let result = await db.collection("documents").insertOne(data);
        if (result.insertedId)
            return result;
    }
    catch (e) {
        console.error(e);
    }
    return undefined;
}

async function findAllDoc(id_user, db) {
    let result = await db.collection("documents").find({ id_user: id_user}).toArray();
    return result;
}

async function deleteDoc(data, db) {
    var o_id = new ObjectId(data.id_doc);
    let foundDoc = await db.collection("documents").findOne({ _id: o_id})
    if (!foundDoc)
        return undefined;
    try {
        db.collection("documents").deleteOne( { "_id" : o_id } );
        return true;
    } catch (e) {
        print(e);
    }
}


module.exports = {
    createDoc,
    findAllDoc,
    deleteDoc
};
