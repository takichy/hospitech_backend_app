const userService = require('../services/admissionService.js')
// const path = require('path');

async function createAdmission(req, res) {
    const { email, firstname, lastname, enterdate, birthdate, doctor, mobile, adresse, contact, mobileContact, postal, town, admission,user } = req.body.data;

    const data = {
        'firstname': firstname,
        'lastname': lastname,
        'email': email,
        'birthdate': birthdate,
        'enterdate': enterdate,
        'doctor': doctor,
        'mobile': mobile,
        'adresse':adresse,
        'contact': contact,
        'mobileContact': mobileContact,
        'postal': postal,
        'town': town,
        'admission': admission,
        'user': user
    }
    console.log("admission back",data)
    const result = await userService.create(data, req.db);

    if (result === undefined)
        return res.status(400).send({ message: 'An error occur during admission creation.' })

    const response = {
        'content': {
            'admission': result
        },
        'message': 'Admission successfully created.'
    }
    return res.status(201).send(response);
}



module.exports = {
    createAdmission
}