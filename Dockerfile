FROM node:14.17.0-alpine3.13

VOLUME [ "/usr/src/app" ]
# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
RUN yarn cache clean

## THE LIFE SAVER
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait

CMD yarn install && /wait && yarn dev
