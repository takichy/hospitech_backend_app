// const Database = require('./database.js');
const mongo = require("mongodb");
const stock = require('../model/stock').Stock;

// let database = new Database();

async function findById(id, database) {
  console.log(id)
  let cursor = await database.collection("stock").findOne({ _id: new mongo.ObjectID(id) });
  console.log(JSON.stringify(cursor));
  return cursor;
}

// getOneSettingDocument('Users', '', UserId.uid);

async function findnoerror(userId) {
  // console.log(" really ? " + await database.getOneUserNoError('Users', userId));
  // return await database.getOneUserNoError('Users', userId);
}

async function findAll(db) {  
  console.log(db)
  return await db.collection("stock").find({}).toArray();
}

async function create(data, db) {

  let result = await db.collection("stock").findOne({refNumber : data.refNumber});

  if (result) {
    return undefined;
  } else {
    try {
      let result = await db.collection("stock").insertOne(data);
      if (result.insertedId)
        return result;
    }
    catch (e) {
      console.error(e);
    }
  }
  return undefined;
}

async function update(database, data, newLicence, orders) {
  try {
    const user = await findByEmail(data.username, database);
    let myorder = user.orders;
    myorder.push(orders);
    return await database.collection("users").updateOne(
      { _id: user._id },
      {
        $set: { "orders": myorder, "licence": newLicence},
        $currentDate: { lastModified: true }
      }
   )
  }
  catch (e) {
    console.error(e);
  }
  return;
}

function remove(userId) {
  // work in progress
  // return database.deleteDocument('Users', userId);
}

async function command(medication, database){    
try {
    const inStock = await findById(medication.id, database);

    return await database.collection("stock").updateOne(
      { _id: inStock._id },
      {
        $set: { 
          'inCommand': inStock.inCommand + medication.command,
        },
        $currentDate: { lastCommand: true }
      }
    )
  }
  catch (e) {
    console.error(e);
  }
  return;
}

async function commandArrived(medication, database){    
  try {
    const inStock = await findById(medication.id, database);
    if(inStock !=  null){
      return await database.collection("stock").updateOne(
        { _id: inStock._id },
        {
          $set: { 
            'inCommand': 0,
            'inStock' : inStock.inCommand + inStock.inStock
          },
          $currentDate: { lastArrival: true }
        }
      )
    }else{
      return "erreur"
    }
  }
  catch (e) {
    console.error(e);
  }
  return;
}

async function stockRemoval(medication, database){    
  try {
    const inStock = await findById(medication.id, database);
    if(inStock !=  null){
      return await database.collection("stock").updateOne(
        { _id: inStock._id },
        {
          $set: { 
            'inStock' : inStock.inStock - 1
          },
          $currentDate: { lastStockRemoval: true }
        }
      )
    }else{
      return "erreur"
    }
  }
  catch (e) {
    console.error(e);
  }
  return;
}



module.exports = {
  findById,
  create,
  update,
  remove,
  findnoerror,
  findAll,
  command,
  commandArrived,
  stockRemoval
};
