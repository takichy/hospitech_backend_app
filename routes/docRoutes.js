const userService = require('../services/userService.js');
const authService = require('../services/authService.js');
const docService = require('../services/docService.js');
const fs = require('fs');
// const path = require('path');
var multer = require('multer');

async function insertDoc(req, res) {
    
    const { id_user, document_blob, document_uri, document_type, document_name, creation_date } = req.body.data;

    const data = {
        'id_user': id_user,
        'document_blob': document_blob,
        'document_uri': document_uri,
        'document_type': document_type,
        'document_name': document_name,
        'creation_date': creation_date
    }
    
    const result = await docService.createDoc(data, req.db);

    if (result === undefined)
        return res.status(400).send({ message: 'An error occur during user creation.' })

    const response = {
        'content': {
            'doc': result
        },
        'message': 'Doc successfully inserted.'
    }

    return res.status(201).send(response);
}

async function getDocsforUser(req, res) {
    const result = await docService.findAllDoc(req.params.id_user, req.db);
    var myJsonString = JSON.stringify(result);
    if (result === undefined)
        return res.status(400).send({ message: 'An error occur during getting all documents.' })
    else
        return res.status(200).send(myJsonString);
}

async function deleteOne(req, res) {
    try {
        const data = {
            'id_doc': req.params.id
        }

        let result = await docService.deleteDoc(data, req.db);
        if (result)
            return res.status(200).send({ message: 'Doc deleted' });
        else
            return res.status(400).send({ message: 'Error when deleting doc' });
    } catch (e) {
        return res.status(400).send({ message: 'Error when deleting doc' });
    }
}

module.exports = {
    insertDoc,
    getDocsforUser,
    deleteOne
}