const hospiService = require('../services/hospiService.js');
const authService = require('../services/authService.js');

async function create(req, res) {
    try {
        const authorization = req.headers.authorization;
        const user = await authService.authenticateToken(authorization);
    
        if (user === undefined) {
            return res.status(400).send({ message: 'Error user is not logged' })
        }
        const data = {
            'user': user.username,
            'markerlatitude': req.body.markerlatitude,
            'markerlongitude': req.body.markerlongitude,
            'centername': req.body.centername,
            'picturecenter': req.body.picturecenter
        }
        await hospiService.createOne(data, req.db);
        return res.status(200).send({ message: 'You are Logged' })
    } catch (e) {
        return res.status(400).send({ message: 'Error when creating Hospital' })
        // return undefined;
    }
}

async function getAll(req, res) {
    
    try {
        const authorization = req.headers.authorization;
        const user = await authService.authenticateToken(authorization);
    
        if (user === undefined) {
            return res.status(400).send({ message: 'Error user is not logged' });
        }
        const data = {
            'user': user.username
        }
        let hospitals = await hospiService.getAll(data, req.db);
        if (hospitals)
            return res.status(200).send({ hospitals: hospitals });
        else 
            return res.status(400).send({ message: 'No hospitals' });
    } catch (e) {
        return res.status(400).send({ message: 'Error when creating Hospital' })
    }
}

async function deleteOne(req, res) {
    try {
        const authorization = req.headers.authorization;
        const user = await authService.authenticateToken(authorization);

        if (user === undefined) {
            return res.status(400).send({ message: 'Error user is not logged' })
        }

        const data = {
            'user': user.username,
            'centerid': req.params.id
        }
        let result = await hospiService.deleteOne(data, req.db);
        if (result)
            return res.status(200).send({ message: 'You are Logged' });
        else
            return res.status(400).send({ message: 'Error when deleting Hospital' });
    } catch (e) {
        return res.status(400).send({ message: 'Error when deleting Hospital' });
        // return undefined;
    }
}

module.exports = {
    create,
    getAll,
    deleteOne
}